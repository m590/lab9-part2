var meanGames = angular.module("meanGames", ['ngRoute'])
    .config(config);

function config($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: "angularjs-app/game-list/games.html",
        controller: "GamesController",
        controllerAs: "vm"
    }).when("/game/:id", {
        templateUrl: "angularjs-app/game-display/game-display.html",
        controller: "GameController",
        controllerAs: "vm"
    }).when("/savegame", {
        templateUrl: "angularjs-app/game-save/game-save.html",
        controller: "GameSaveController",
        controllerAs: "vm"
    });
}