meanGames.controller("GameController", GameController);

function _getStarsArray(rate) {
    return new Array(rate);
}

function GameController(GameService, $routeParams) {
    const vm = this;

    const gameId = $routeParams.id;
    GameService.getGame(gameId).then(function (res) {
        vm.game = res;
        vm.rating = _getStarsArray(vm.game.rate);
    });

}