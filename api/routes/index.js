const express = require('express');
const gameCtrl = require('./../controller/games.controller.js');
const publisherCtrl = require('./../controller/publisher.ctrl');

const router = express.Router();

router.route("/games")
    .get(gameCtrl.gamesGetAll)
    .post(gameCtrl.gamesAddOne);

router.route("/games/:gameId")
    .get(gameCtrl.gamesGetOne)
    .put(gameCtrl.gamesFullUpdateOne)
    .patch(gameCtrl.gamesPartialUpdateOne)
    .delete(gameCtrl.gamesDeleteOne);

router.route("/games/:gameId/publisher")
    .get(publisherCtrl.publisherGetOne)
    .post(publisherCtrl.publisherAddOne);

// router module - g export hiine
module.exports = router;